//中文
const zh = {
  article: "文章",
  archive: "归档",
  knowledge: "知识小册",
  leave: "留言",
  about: "关于",
};

export default zh;
