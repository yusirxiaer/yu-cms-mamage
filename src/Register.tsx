import { Button, Form, Input, message } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Login.less";
import { RegisterApi } from "request/api";
const loginPic = require("./assets/logo.png");
interface IRegisterLogin {
  username: string;
  password: string;
  repassword?: string;
}
interface IRes {
  errCode?: number;
  message?: string;
  data?: any;
}

export default function Register() {
  const navigate = useNavigate();

  const onFinish = (values: IRegisterLogin) => {
    console.log("Success:", values);
    // //获取用户名和密码
    let { username, password, repassword } = values;
    if (password !== repassword) {
      message.error("请输入相同的密码");
      return;
    }
    RegisterApi({ username, password }).then((res: IRes) => {
      console.log(res);
      if (res.errCode === 0) {
        message.success(res.message);
        //跳转登录页
        setTimeout(() => {
          navigate("/login");
        }, 1500);
      } else {
        message.error(res.data);
      }
    });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className='login_box'>
      <img src={loginPic} className='logo' alt='' />
      <Form
        name='basic'
        wrapperCol={{ span: 24 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete='off'
      >
        <Form.Item
          name='username'
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input
            prefix={<UserOutlined className='site-form-item-icon' />}
            placeholder='请输入用户名'
          />
        </Form.Item>

        <Form.Item
          name='password'
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password
            prefix={<LockOutlined className='site-form-item-icon' />}
            placeholder='请输入密码'
          />
        </Form.Item>

        <Form.Item
          name='repassword'
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password
            prefix={<LockOutlined className='site-form-item-icon' />}
            placeholder='请再次输入密码'
          />
        </Form.Item>

        <Form.Item>
          <Link to='/login'>已有账号？返回登录</Link>
        </Form.Item>

        <Form.Item>
          <Button type='primary' block size='large' htmlType='submit'>
            注册
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
