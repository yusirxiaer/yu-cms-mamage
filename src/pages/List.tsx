import { Button, message, Pagination, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GetArticleListApi, DeleteArticleApi } from "request/api";
import moment from "moment";
import { Trans, useTranslation } from "react-i18next";

interface IData {
  key: number;
  title: React.ReactNode;
  time: string;
  action: React.ReactNode;
}
interface IItem {
  title: string;
  sub_title: string;
  date: string;
  id: number;
}
//标题与副标题组件
const TitleComp = (props: { title: string; subTitle?: string }) => (
  <>
    <div>
      <a href='!#'>{props.title}</a>
    </div>
    <p style={{ color: "#999" }}>{props.subTitle}</p>
  </>
);

const ActionBtn = (props: {
  current: number;
  id: number;
  getListFn: (current: number, pageSize: number) => void;
}) => {
  const navigate = useNavigate();
  //点击了编辑按钮
  const goToEdit = () => {
    //携带id跳转到edit页面
    navigate("/edit/1");
  };

  const deleteFn = () => {
    DeleteArticleApi({ id: props.id }).then((res: any) => {
      if (res.errCode === 0) {
        message.success(res.message);
        props.getListFn(1, 10);
      } else {
        message.error(res.message);
      }
    });
  };
  return (
    <>
      <Button type='primary' style={{ marginRight: "20px" }} onClick={goToEdit}>
        编辑
      </Button>
      <Button type='primary' danger onClick={deleteFn}>
        删除
      </Button>
    </>
  );
};

export default function List() {
  //列表数组
  const [data, setData] = useState<IData[]>([]);
  //分页组件的总数据条数
  const [total, setTotal] = useState(0);
  //分页当前页码
  const [current, setCurrent] = useState(1);
  const columns = [
    {
      title: "文章标题",
      dataIndex: "title",
      width: "60%",
    },
    {
      title: "发布时间",
      dataIndex: "time",
    },
    {
      title: "操作",
      dataIndex: "action",
    },
  ];

  const getListFn = (current: number, pageSize: number) => {
    GetArticleListApi({
      pageSize,
      current,
    }).then((res) => {
      console.log(res);
      setTotal(res.data.total);
      setCurrent(res.data.current);
      let newarr: IData[] = [];
      res.data.arr.map((item: IItem) => {
        let obj = {
          key: item.id,
          title: <TitleComp title={item.title} subTitle={item.sub_title} />,
          time: moment(item.date).format("YYYY-MM-DD hh:mm:ss"),
          action: (
            <ActionBtn id={item.id} getListFn={getListFn} current={current} />
          ),
        };
        newarr.push(obj);
      });
      setData(newarr);
    });
  };

  useEffect(() => {
    getListFn(current, 10);
  }, []);

  const onShowSizeChange = (current: number, pageSize: number) => {
    console.log(current, pageSize);
    getListFn(current, pageSize);
  };
  const { t, i18n } = useTranslation();
  return (
    <div>
      <h1>{t("footer.detail")}</h1>
      <h1>
        <Trans>home.new_arrival</Trans>
      </h1>
      <button
        onClick={() => i18n.changeLanguage(i18n.language == "en" ? "zh" : "en")}
      >
        {i18n.language == "en" ? "zh" : "en"}
      </button>
      <Table columns={columns} dataSource={data} pagination={false} />
      <Pagination
        style={{ float: "right" }}
        showSizeChanger
        onChange={onShowSizeChange}
        defaultCurrent={1}
        total={total}
      />
    </div>
  );
}
