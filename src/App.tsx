import React, { useEffect, useState } from "react";
import { Outlet, Link, useLocation, useNavigate } from "react-router-dom";
import { Layout, Menu, Breadcrumb } from "antd";
import {
  ReadOutlined,
  UserOutlined,
  EditOutlined,
  FormOutlined,
} from "@ant-design/icons";
import "./App.less";
import MyHeader from "components/MyHeader";
import { connect } from "react-redux";
import { Dispatch } from "redux";

const { SubMenu } = Menu;
const { Content, Sider } = Layout;
interface IProps {
  headKey: number;
  changeKeyFn: () => void;
}
function App(props: IProps) {
  // 定义侧边栏当前项的值
  const [asideKey, setAsideKey] = useState("0");
  const [bread, setBread] = useState("");
  const navigate = useNavigate();
  const location = useLocation();

  // 监听路由的变化，从而修改侧边栏当前项
  useEffect(() => {
    if (location.pathname === "/") {
      //根路径重定向到/1is
      navigate("/list");
    }
    switch (location.pathname) {
      case "/list":
        setAsideKey("1");
        setBread("查看文章列表");
        break;
      case "/edit":
        setAsideKey("2");
        setBread("文章编辑");

        break;
      case "/means":
        setAsideKey("3");
        setBread("修改资料");

        break;
      case "/namelist":
        setAsideKey("4-1");
        setBread("小编名单");
        break;
      default:
        setAsideKey("0");
        break;
    }
    //只要路径中有/edit的字符，就让文章编辑呈现当前项
    if (location.pathname.includes("/edit")) {
      setAsideKey("2");
    }
  }, [location.pathname]);

  const changeusername = () => {
    //把localStorage中的cms-username直接改了
    localStorage.setItem("cms-username", "赵云");
    //希望：Header组件更新1
    props.changeKeyFn();
  };

  return (
    <Layout className='container'>
      <MyHeader key={props.headKey} />
      <Layout className='container_content'>
        <Sider width={200}>
          <Menu
            mode='inline'
            theme='dark'
            selectedKeys={[asideKey]}
            defaultOpenKeys={["4"]}
            style={{ height: "100%", borderRight: 0 }}
          >
            <Menu.Item key='1'>
              <Link to={"/list"}>
                <ReadOutlined /> 查看文章列表
              </Link>
            </Menu.Item>
            <Menu.Item key='2'>
              <Link to={"/edit"}>
                <EditOutlined /> 文章编辑
              </Link>
            </Menu.Item>
            <Menu.Item key='3'>
              <Link to={"/means"}>
                <FormOutlined /> 修改资料
              </Link>
            </Menu.Item>
            <SubMenu
              key='4'
              icon={<UserOutlined />}
              title='管理员'
              style={{
                display:
                  localStorage.getItem("cms-player") === "vip"
                    ? "block"
                    : "none",
              }}
            >
              <Menu.Item key='4-1'>
                <Link to={"/namelist"}>
                  <ReadOutlined />
                  小编名单
                </Link>
              </Menu.Item>
            </SubMenu>
            <Menu.Item key='5'>
              <Link to={"/editable"}>
                <FormOutlined /> 可编辑表格案例
              </Link>
            </Menu.Item>
            <Menu.Item key='5'>
              <Link to={"/editablerow"}>
                <FormOutlined /> 可编辑行表格案例
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout style={{ padding: "0 24px 24px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>
              <Link to={"/"}>首页</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>{bread}</Breadcrumb.Item>
          </Breadcrumb>
          <Content className='mycontent'>
            {/* <Button onClick={changeusername}>
              修改用户名,改变key重新渲染MyHeader组件
            </Button> */}
            <Outlet />
          </Content>
        </Layout>
      </Layout>
      <footer
        style={{
          textAlign: "center",
          color: "#fff",
          height: "70px",
          lineHeight: "70px",
          background: "#001529",
        }}
      >
        Respect | Copyright 2022 Author你单排吧
      </footer>
    </Layout>
  );
}

//state的映射
const mapStateToProps = (state: { key: number }) => {
  return {
    headKey: state.key,
  };
};

//dispatch的映射
// const mapDispatchToProps = (dispatch: Dispatch) => {
//   return {
//     changeKeyFn() {
//       dispatch({ type: "ChangeKey" });
//     },
//   };
// };
export default connect(mapStateToProps)(App);
