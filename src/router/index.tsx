import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import React, { Suspense, lazy } from "react";
import Loading from "../components/Loading";
import App from "App";

interface Iroute {
  path: string;
  component: React.FC<any>;
  children?: Iroute[];
}

let routeArr: Iroute[] = [
  {
    path: "/",
    component: App,
    children: [
      { path: "/edit", component: lazy(() => import("../pages/Edit")) },
      { path: "/edit/:id", component: lazy(() => import("../pages/Edit")) },
      { path: "/list", component: lazy(() => import("../pages/List")) },
      { path: "/means", component: lazy(() => import("../pages/Means")) },
      { path: "/namelist", component: lazy(() => import("../pages/NameList")) },
      {
        path: "/editable",
        component: lazy(() => import("../pages/EditTable")),
      },
      {
        path: "/editablerow",
        component: lazy(() => import("../pages/EditableTable")),
      },
    ],
  },
  { path: "/login", component: lazy(() => import("Login")) },
  { path: "/register", component: lazy(() => import("Register")) },
];

const MyRouter = () => (
  <Router>
    <Suspense fallback={<Loading />}>
      <Routes>
        {routeArr.map((item, index) => {
          return item.children ? ( //有子路由
            <Route key={index} path={item.path} element={<item.component />}>
              {item.children.map((e, i) => (
                <Route key={i} path={e.path} element={<e.component />}></Route>
              ))}
            </Route>
          ) : (
            <Route
              key={index}
              path={item.path}
              element={<item.component />}
            ></Route>
          );
        })}
      </Routes>
    </Suspense>
  </Router>
);

export default MyRouter;
