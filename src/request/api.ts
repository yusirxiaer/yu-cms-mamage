import request from "./request";

interface IRegisterLogin {
  username: string;
  password: string;
}

// 注册
export const RegisterApi = (params: IRegisterLogin) =>
  request.post("/register", params);
//登录接口
export const LoginApi = (params: IRegisterLogin) =>
  request.post("/login", params);
// 获取用户信息
export const UserInfoApi = () => request.get("/userInfo");

// 修改用户信息
//username和password目前是可传可不传1
export const ChangeUserInfoApi = (params: IRegisterLogin) =>
  request.post("/userInfo", params);

interface ArticleQuery {
  pageSize: number;
  current: number;
}
// 获取文章列表
export const GetArticleListApi = (params: ArticleQuery) =>
  request.get("/article/list", { params });

//根据id获取文章
export const GetArticleByIdApi = (params: { id: number }) =>
  request.post("/article/list/id", params);

//文章编辑接口
interface IEditArticle {
  title: string;
  subTitle?: string;
  content: string;
  id: number;
}
export const EditArticleApi = (params: IEditArticle) =>
  request.post("/article/list/edit", params);

//文章删除接口
export const DeleteArticleApi = (params: { id: number }) =>
  request.post("/article/delete", params);

//文章添加
export const AddArticleApi = (params: IEditArticle) =>
  request.post("/article/add", params);

// 获取小编名单
export const EditorApi = () => request.get("/namelist");

//修改小编的编辑权限
interface IChangeEditor {
  id: number;
  open: number;
}

export const ChangeEditorApi = (params: IChangeEditor) =>
  request.post("/namelist", params);
