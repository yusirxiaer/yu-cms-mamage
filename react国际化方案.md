# React国际化react-i18next

## 简介

**react-i18next** 是基于 `i18next` 的一款强大的国际化框架，可以用于 `react` 和 `react-native` 应用，是目前非常主流的国际化解决方案。

`i18next` 有着以下优点：

- 基于i18next不仅限于react，学一次就可以用在其它地方
- 提供多种组件在hoc、hook和class的情况下进行国际化操作
- 适合服务端的渲染
- 历史悠久，始于2011年比大多数的前端框架都要年长
- 因为历史悠久所以更成熟，目前还没有i18next解决不了的国际化问题
- 有许多插件的支持，比如可以用插件检测当前系统的语言环境，从服务器或者文件系统加载翻译资源

## 安装

需要同时安装 `i18next` 和 `react-i18next` 依赖：

```
npm install react-i18next i18next --save`
或
`yarn add react-i18next i18next --save
```

------

配置

在`src`下新建`i18n`文件夹，以存放国际化相关配置

`i18n`中分别新建三个文件：

- `config.ts`：对 i18n 进行初始化操作及插件配置
- `en.json`：英文语言配置文件
- `zh.json`：中文语言配置文件

![在这里插入图片描述](https://www.yht7.com/upload/image/2021/10/22/2021102214270842.png) 

en.json

```json
{
    "header": {
        "register":"Register",
        "signin":"Sign In",
        "home": "Home"
    },
    "footer": {
        "detail" : "All rights reserved @ React"
    },
    "home": {
        "hot_recommended": "Hot Recommended",
        "new_arrival": "New arrival",
        "joint_venture": "Joint Venture"
    }
}
```

zh.json

```json
{
    "header": {
        "register":"注册",
        "signin":"登陆",
        "home": "首页"
    },
    "footer": {
        "detail" : "版权所有 @ React"
    },
    "home": {
        "hot_recommended": "爆款推荐",
        "new_arrival": "新品上市",
        "joint_venture": "合作企业"
    }
}
```

config.ts

```tsx
import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import translation_en from "./en.json";
import translation_zh from "./zh.json";

const resources = {
    en: {
        translation: translation_en,
    },
    zh: {
        translation: translation_zh,
    },
};

i18n.use(initReactI18next).init({
    resources,
    lng: "zh",
    interpolation: {
        escapeValue: false,
    },
});

export default i18n;
```

## 使用

引用配置文件

在入口文件`index.tsx`中引用`i18n`的配置文件 ：`import "./i18n/config";`

```tsx
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "./i18n/config"; // 引用配置文件

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById("root")
);
```

## 在组件中使用

方法一

在 **类组件** 中使用`withTranslation` **高阶函数(HOC)** 来完成语言配置的数据注入

```tsx
import React from "react";
import styles from "./Home.module.css";

// 引入HOC高阶函数withTranslation 和 i18n的ts类型定义WithTranslation 
import { withTranslation, WithTranslation } from "react-i18next"

class HomeComponent extends React.Component<WithTranslation> {
    render() {
        const { t } = this.props;
        return <>
           <h1>{t("header.home")}</h1>
           <ul>
               <li>{t("home.hot_recommended")}</li>
               <li>{t("home.new_arrival")}</li>
               <li>{t("home.joint_venture")}</li>
           </ul>
        </>
    }
}

export const Home = withTranslation()(HomeComponent); // 使用withTranslation高阶函数来完成语言配置的数据注入
```

方法二

在 **函数式组件** 中使用`useTranslation` 的 **hook** 来处理国际化

```tsx
import React from "react";
import { useTranslation, Trans } from "react-i18next"

export const Home: React.FC = () => {
    const { t } = useTranslation()
    return (
		<div>
			<h1>{t("header.home")}</h1>
			<ul>
				<li>{t("home.hot_recommended")}</li>
				{/* 还有一种方式 */}
				<li><Trans>home.new_arrival</Trans></li>
			</ul>
		</div>    
    );
};
```

切换语言

```tsx
import i18n from "i18next";

const changeLanguage= (val) => {
	i18n.changeLanguage(val); // val入参值为"en"或"zh"
};
```

或

```tsx
import React from "react";
import { useTranslation } from "react-i18next"

export const Home: React.FC = () => {
    const { t, i18n } = useTranslation()
    return (
		<button onClick={()=>i18n.changeLanguage(i18n.language=="en"?"zh":"en")}>{i18n.language=="en"?"zh":"en"}</button>
    );
};
```



----

上次写完react-intl的国际化入门之后，发现react-intl有一个问题，那就是不支持hooks，而react-i18next是支持react的hooks写法的。i18next也是一个很完备的国际化方案，为 React、AngularJS、Vue.js 等前端框架创建了集成。

## 1.安装

```
npm install --save react-i18next
```

## 2.配置多语言映射关系

推荐使用json格式，可以在项目中新建文件夹locale统一管理,例如：

![企业微信截图_16443890902121.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/eaa8eadd0d164babb566762edfb84a38~tplv-k3u1fbpfcp-zoom-in-crop-mark:1304:0:0:0.awebp?)

在index中,导入各语言包,并导入i18n执行初始化

```ts
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import enUS from './en_US.json';
import zhCN from './zh_CN.json';

const resources = {
	'en-US': {
		translation: enUS,
	},
	'zh-CN': {
		translation: zhCN,
	},
};

i18n.use(initReactI18next).init({
	resources,
	lng: 'zh-CN',
	interpolation: {
		escapeValue: false,
	},
});

export default i18n;
复制代码
```

## 3.导入文件

```tsx
import React, { Component } from "react";
import ReactDOM from "react-dom";
import App from './App';
import './locale/index'; // 在这里导入

ReactDOM.render(
  <App />,
  document.getElementById("root")
);
复制代码
```

## 4.使用

react-i18next提供了多种使用方式

### 1.hooks

在函数式组件中，使用hook是最为方便的，用t（）包裹需要国际化的内容即可

```jsx
import * as React from 'react';
import { useTranslation } from 'react-i18next';
const Hello: React.FC = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="hello">
            <h1>{t('hello')}</h1>
        </div>
    );
};

export default Hello;

复制代码
```

### 2.高阶组件HOC

高阶组件也是常用的方法之一，用withTranslation处理组件，通过props传入t函数和i18n实例

```tsx
import * as React from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';
const Hello: React.FC<WithTranslation> = ({ t, i18n }) => {
    return (
        <div className="hello">
            <h1>{t('hello')}</h1>
        </div>
    );
};

export default withTranslation()(Hello);
复制代码
```

### 3.Translation组件

Translation可以让包裹其中的部分获取到t函数和i18n实例

```tsx
import * as React from 'react';
import { Translation } from 'react-i18next';
const Hello: React.FC = () => {
    return (
        <div className="hello">
                <Translation>
                    {
                        (t, { i18n }) => <h1>{t('hello')}</h1>
                    }
                </Translation>
        </div>
    );
};

export default Hello;
复制代码
```

### 4.Trans组件

```tsx
import * as React from 'react';
import { Trans } from 'react-i18next';
const Hello: React.FC = () => {
    return (
        <div className="hello">
            <Trans> hello </Trans>
        </div>
    );
};

export default Hello;
```


作者：虞啊鱼
链接：https://juejin.cn/post/7062902741714075656
来源：稀土掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。